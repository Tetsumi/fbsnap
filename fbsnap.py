###########################################################################
#
#  FBSnap
#
#  Copyright 2015 Tetsumi <tetsumi@vmail.me>
#  
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###########################################################################

import ctypes
import argparse
from fcntl import ioctl

# Data structures from Linux/include/uapi/linux/fb.h

#
# fb_bitfield
#
class Bitfield(ctypes.Structure):
    _fields_ = [
        ('offset',    ctypes.c_uint32),
        ('length',    ctypes.c_uint32),
        ('msb_right', ctypes.c_uint32)
    ]

#
# fb_var_screeninfo 
#
class Varinfo(ctypes.Structure):
    _fields_ = [
        ('xres',           ctypes.c_uint32),
        ('yres',           ctypes.c_uint32),
        ('xres_virtual',   ctypes.c_uint32),
        ('yres_virtual',   ctypes.c_uint32),
        ('xoffset',        ctypes.c_uint32),
        ('yoffset',        ctypes.c_uint32),
        ('bits_per_pixel', ctypes.c_uint32),
        ('grayscale',      ctypes.c_uint32),
        ('red',            Bitfield),
        ('green',          Bitfield),
        ('blue',           Bitfield),
        ('transp',         Bitfield),
        ('nonstd',         ctypes.c_uint32),
        ('activate',       ctypes.c_uint32),
        ('height',         ctypes.c_uint32),
        ('width',          ctypes.c_uint32),
        ('accel_flags',    ctypes.c_uint32),
        ('pixclock',       ctypes.c_uint32),
        ('left_margin',    ctypes.c_uint32),
        ('right_margin',   ctypes.c_uint32),
        ('upper_margin',   ctypes.c_uint32),
        ('lower_margin',   ctypes.c_uint32),
        ('hsync_len',      ctypes.c_uint32),
        ('vsync_len',      ctypes.c_uint32),
        ('sync',           ctypes.c_uint32),
        ('vmode',          ctypes.c_uint32),
        ('rotate',         ctypes.c_uint32),
        ('colorspace',     ctypes.c_uint32),
        ('reserved',       ctypes.c_uint32 * 4),     
    ]

#
# fb_fix_screeninfo
#
class Fixedinfo(ctypes.Structure):
    _fields_ = [
        ('id',           ctypes.c_char * 16),
        ('smem_start',   ctypes.c_ulong),
        ('smem_len',     ctypes.c_uint32),
        ('type',         ctypes.c_uint32),
        ('type_aux',     ctypes.c_uint32),
        ('visual',       ctypes.c_uint32),
        ('xpanstep',     ctypes.c_uint16),
        ('ypanstep',     ctypes.c_uint16),
        ('ywrapstep',    ctypes.c_uint16),
        ('line_length',  ctypes.c_uint32),
        ('mmio_start',   ctypes.c_ulong),
        ('mmio_len',     ctypes.c_uint32),
        ('accel',        ctypes.c_uint32),
        ('capabilities', ctypes.c_uint16),
        ('reserved',     ctypes.c_uint16 * 2)
    ]

FBIOGET_VSCREENINFO = 0x4600
FBIOGET_FSCREENINFO = 0x4602

def fb_to_ppm (rect, color_pos, line_length, output_filename):
    with open(output_filename, "wb") as out:
        print("Writing to", output_filename)
        
        # write header
        hd = "P6\n{0} {1}\n255\n".format(rect.width, rect.height)
        out.write(bytes(hd, "ascii"))
        
        # write pixels
        for y in range(rect.top, rect.bottom):
            for x in range(rect.left, rect.right):
                position = (y * line_length) + (x * 4)
                r = pixels[position + color_pos[0]]
                g = pixels[position + color_pos[1]]
                b = pixels[position + color_pos[2]]
                out.write(bytes([r, g, b]))

class Rectangle:
    def __init__ (self, left, top, right, bottom):
        if left >= right:
            raise ValueError("Bad rectangle: Left > Right")
        if top >= right:
            raise ValueError("Bad rectangle: Top > Bottom")
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom
        self.width = right - left
        self.height = bottom - top

    def str (self):
        return "Rectangle({}, {}, {}, {})".format(self.left,
                                                  self.top,
                                                  self.right,
                                                  self.bottom)
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="FBSnap",
                                     description='Take screenshot of framebuffer.')
    
    parser.add_argument("-t", "--top",    type=int, default=0)
    parser.add_argument("-b", "--bottom", type=int, default=0)
    parser.add_argument("-l", "--left",   type=int, default=0)
    parser.add_argument("-r", "--right",  type=int, default=0)
    parser.add_argument("-d", "--device", type=str, default="/dev/fb0")
    parser.add_argument("-o", "--out",    type=str, default="screen.ppm")
    
    parser.add_argument("-v",
                        "--version",
                        action="version",
                        version="%(prog)s v1.0.0")
                        
    args = parser.parse_args()
    
    with open(args.device, "rb") as fbdev:
        vinfo = Varinfo()
        finfo = Fixedinfo()
        ioctl(fbdev, FBIOGET_VSCREENINFO, vinfo)
        ioctl(fbdev, FBIOGET_FSCREENINFO, finfo)
        pixels = fbdev.read()

    print("FBSnap 1.0.0\n")

    if 0 >= args.bottom:
        args.bottom = vinfo.yres

    if 0 >= args.right:
        args.right = vinfo.xres

    rect      = Rectangle(args.left, args.top, args.right, args.bottom)
    red_pos   = vinfo.red.offset   // 8
    green_pos = vinfo.green.offset // 8
    blue_pos  = vinfo.blue.offset  // 8
    color_pos = (red_pos, green_pos, blue_pos)
    
    fb_to_ppm(rect, color_pos, finfo.line_length, args.out)
    
    print("Done!")
    
        
